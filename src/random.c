#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>
#include <assert.h>

int
main (int argc, char *argv[])
{
  char words[16764][16];
  int n = 0, c, ret;
  long seed = 0;

  if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
    {
      seed = strtoul (argv[2], NULL, 10);
      argc -= 2;
      argv += 2;
    }
  
  while ((c = getchar ()) != EOF)
    {
      ungetc (c, stdin);
      ret = scanf ("%s\n", words[n]);
      assert (ret == 1);
      n++;
    }
  if (seed == 0)
    {
      struct timeval tv[1];
      gettimeofday (tv, NULL);
      seed = getpid () + tv->tv_sec + tv->tv_usec;
      printf ("seed = %lu\n", seed);
      srand48 (seed);
    }
  n = lrand48 () % n;
  printf ("%s\n", words[n]);
}
