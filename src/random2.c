/* affiche un tirage aléatoire parmi une liste de mots en entrée,
   et affiche aussi le nombre d'anagrammes */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>

typedef struct {
  char tirage[16];
  char *anagrammes; /* 16*size characters */
  unsigned long size;
  double prob;
  int success; /* number of consecutive successes */
} tirage_struct;

typedef tirage_struct tirage_t[1];

static void
trie_mot (char *t)
{
  unsigned int i, j;
  char c;
  
  for (i = 1; i < strlen (t); i++)
    for (j = i; j-- > 0;)
      if (t[j] > t[j+1])
        {
          c = t[j];
          t[j] = t[j+1];
          t[j+1] = c;
        }
}

/* assume c is lower case */
static int
nb_points (char c)
{
  /*           a b c d e f g h i j  k l m n o p q r s t u v  w  x  y  z */
  int T[26] = {1,3,3,2,1,4,2,4,1,8,10,1,2,1,1,3,8,1,1,1,1,4,10,10,10,10};
  assert ('a' <= c && c <= 'z');
  return T[c - 'a'];
}

/* assume word is sorted */
static double
proba (char *t)
{
  double p = 1.0;
  unsigned int i, j;
  int freq[26] = {9,2,2,3,15,2,2,2,8,1,1,5,3,6,6,2,1,6,6,6,6,2,1,1,1,1};

  for (i = 0; i < strlen (t);)
    {
      int f = freq[t[i] - 'a'];
      p *= f;
      for (j = i + 1; j < strlen (t) && t[j] == t[i]; j++)
        {
          if (j - i < f)
            p *= (double) (f - (j - i));
          p /= (double) (j + 1 - i);
        }
      i = j;
    }
  return p / 18466953120.0; /* p / binomial(102,7) */
}

static void
permute (char *t, char *s)
{
  unsigned long i, j, n = strlen (s);
  char c;
  
  strcpy (t, s);
  for (i = n-1; i > 0; i--)
    {
      /* permute t[i] with t[0] or t[1] or ... or t[i] */
      j = lrand48 () % i;
      c = t[i];
      t[i] = t[j];
      t[j] = c;
    }
  t[n] = '\0';
}

unsigned long
isprime (unsigned long n)
{
  unsigned long p;

  if (n == 1)
    return 0;

  if ((n % 2) == 0)
    return n == 2;

  for (p = 3; p * p <= n; p += 2)
    if ((n % p) == 0)
      return 0;

  return 1;
}

static unsigned long
nextprime (unsigned long n)
{
  while (isprime (n) == 0)
    n ++;
  return n;
}

/* https://stackoverflow.com/questions/448944/c-non-blocking-keyboard-input */
#include <termios.h>

void stdin_set(int cmd)
{
  struct termios t;
  tcgetattr(1,&t);
  switch (cmd) {
  case 1:
    t.c_lflag &= ~ICANON;
    break;
  default:
    t.c_lflag |= ICANON;
    break;
  }
  tcsetattr(1,0,&t);
}

/* return the number of successes (0 by default) */
int
read_word (FILE *fp, char *s)
{
  int c;
  
  while (1)
    {
      c = getc (fp);
      if ('a' <= c && c <= 'z')
        *s++ = c;
      else
        break;
    }
  *s = '\0';
  if (c == '\n')
    {
      ungetc (c, fp);
      return 0;
    }
  else
    {
      int suc;
      int ret = fscanf (fp, "%d", &suc);
      assert (ret == 1);
      return suc;
    }
}

int num_successes = 3;

static void
update_file (char *s, tirage_t *T, int size)
{
  char t[1024];
  strcpy (t, s);
  strcpy (t + strlen (s), "-tmp");
  FILE *fp = fopen (t, "w");
  int remains = 0;
  for (int i = 0; i < size; i++)
    {
      /* remove tirage after num_successes consecutive successes */
      if (T[i]->success < num_successes)
        {
          remains ++;
          if (T[i]->success == 0)
            for (int j = 0; j < T[i]->size; j++)
              fprintf (fp, "%s\n", T[i]->anagrammes + 16 * j);
          else
            for (int j = 0; j < T[i]->size; j++)
              fprintf (fp, "%s:%d\n", T[i]->anagrammes + 16 * j, T[i]->success);
        }
    }
  printf ("Remains %d tirages out of %d initially (removed %d)\n",
          remains, size, size - remains);
  printf ("cp %s %s\n", t, s);
  fclose (fp);
}

int
main (int argc, char *argv[])
{
  tirage_t *T = NULL;
  unsigned long size = 0, alloc = 0, i, j, nwords = 0;
  char c, s[16], t[16];
  int ret;
  int seed = 0;
  FILE *fp;
  int anagrams = 0; /* if non-zero, restrict to this number of anagrams */
  double prob = 0.0; /* take into account probability */
  int wanted_points = 0; /* if not 0, sum of points must equal wanted_points */
  int ntries = 0;

  while (argc > 1 && argv[1][0] == '-')
    {
      if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
          seed = strtoul (argv[2], NULL, 10);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-anagrams") == 0)
        {
          anagrams = atoi (argv[2]);
          assert (anagrams > 0);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-points") == 0)
        {
          wanted_points = atoi (argv[2]);
          assert (wanted_points > 0);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 2 && strcmp (argv[1], "-prob") == 0)
        {
          prob = 1.0;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-ntries") == 0)
        {
          ntries = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nsuccesses") == 0)
        {
          num_successes = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }

  /* set default value of ntries */
  if (ntries == 0)
    ntries = (anagrams == 0) ? 100 : 100 / anagrams;

  assert (argc == 2);
  fp = fopen (argv[1], "r");

  while ((c = fgetc (fp)) != EOF)
    {
      ungetc (c, fp);
      /* one line is either 'word' or 'word:nnn' */
      int suc = read_word (fp, s);
      nwords ++;

      /* ignore end of line */
      while ((c = fgetc (fp)) != EOF && c != '\n');

      /* convert to lowercase */
      int points = 0;
      for (i = 0; i < strlen (s); i++)
        {
          s[i] = tolower (s[i]);
          points += nb_points (s[i]);
        }

      if (wanted_points != 0 && points != wanted_points)
        continue;

      strcpy (t, s);
      trie_mot (t);

      /* search if tirage already in T */
      for (i = 0; i < size; i++)
        if (strcmp (t, T[i]->tirage) == 0)
          {
            /* if tirage already in T, check 'success' agrees */
            assert (T[i]->success == suc);
            break;
          }
      if (i == size) /* tirage not yet in T */
        {
          if (size >= alloc)
            {
              alloc = 2 * alloc + 1;
              T = realloc (T, alloc * sizeof (tirage_t));
            }
          assert (i < alloc);
          strcpy (T[i]->tirage, t);
          T[i]->anagrammes = NULL;
          T[i]->size = 0;
          T[i]->success = suc;
          size ++;
        }
      /* now search if word was already in T[i].anagrammes */
      for (j = 0; j < T[i]->size; j++)
        {
          if (strcmp (s, T[i]->anagrammes + 16 * j) == 0)
            break;
        }
      if (j < T[i]->size)
        printf ("Warning: duplicate word %s\n", s);
      else /* j = T[i].size */
        {
          T[i]->size ++;
          if (T[i]->anagrammes == NULL)
            T[i]->anagrammes = malloc (16);
          else
            T[i]->anagrammes = realloc (T[i]->anagrammes, 16 * T[i]->size);
          strcpy (T[i]->anagrammes + 16 * j, s);
        }
    }
  fclose (fp);

  /* if anagrams > 0, check there is at least one possible draw */
  unsigned long count = 0;
  if (anagrams > 0)
    {
      for (i = 0; i < size; i++)
        count += T[i]->size == anagrams;
      if (count == 0)
        {
          fprintf (stderr, "Error, no draw with %d anagram(s)", anagrams);
          if (wanted_points != 0)
            fprintf (stderr, " and %d point(s)", wanted_points);
          fprintf (stderr, "\n");
          exit (1);
        }
    }
  else
    count = size;

  if (prob != 0) /* determine largest probability */
    {
      double maxprob = 0;
      int imax = 0;
      for (i = 0; i < size; i++)
        {
          if (anagrams == 0 || T[i]->size == anagrams)
            {
              T[i]->prob = proba (T[i]->tirage);
              if (T[i]->prob > maxprob)
                {
                  maxprob = T[i]->prob;
                  imax = i;
                }
            }
        }
      printf ("Maximum probability %.4f%% with %s\n", 100 * maxprob,
              T[imax]->tirage);
      prob = maxprob;
    }

  if (seed == 0)
    {
      struct timeval tv[1];
      gettimeofday (tv, NULL);
      seed = getpid () + tv->tv_sec + tv->tv_usec;
    }
  srand48 (seed);
  printf ("Using seed %d\n", seed);

  printf ("Read %lu word(s), %lu tirage(s), start %d tries\n", nwords, count,
          ntries);

  /* permute entries */
  tirage_t tmp;
  for (i = size; --i > 0;)
    {
      /* permute T[i] with T[j] with j < i */
      j = lrand48 () % i;
      memcpy (tmp, T[i], sizeof (tirage_struct));
      memcpy (T[i], T[j], sizeof (tirage_struct));
      memcpy (T[j], tmp, sizeof (tirage_struct));
    }

  unsigned long p = nextprime (size);
  unsigned long g; /* generator */
  unsigned long n = 0;
  do
    g = (lrand48 () >> 15) % p;
  while (g == 0);
  i = (lrand48 () >> 15) % p; /* initial value */
  printf ("p=%lu g=%lu\n", p, g);

  stdin_set (1);

  while (ntries != 0)
    {
      i = (i + g) % p;
      if (i >= size)
        continue;
      permute (t, T[i]->tirage);
      if (anagrams > 0 && T[i]->size != anagrams)
        continue;
      double d = drand48 ();
      // printf ("%s, pr=%f, d=%f\n", T[i]->tirage, T[i]->prob / prob, d);
      if (prob && d > T[i]->prob / prob)
        continue;
      n ++;
      if (n > count)
        n -= count;
      printf ("%lu %s", n, t);
      if (T[i]->size > 1)
        printf (" ### %lu ###", T[i]->size);
      if (prob > 0)
        printf (" %.0f%%", 100.0 * proba (T[i]->tirage) / prob);
      c = getchar ();
      for (j = 0; j < T[i]->size; j++)
        printf ("%s ", T[i]->anagrammes + 16 * j);
      //      trie_mot (t);
      printf ("\n");
      printf ("ok (y/n)? ");
      fflush (stdout);
      c = getchar ();
      if (c != 'n')
        T[i]->success ++;
      else
        T[i]->success = 0;
      if (c != '\n')
        printf ("\n");
      printf ("\n");
      ntries --;
    }

  update_file (argv[1], T, size);

    return 0;
}
