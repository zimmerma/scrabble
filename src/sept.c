#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>

/* sept lettres a un point */

#define MAXN 120
#define MAX 72 /* maximal numbers of words (7 or 8 letters) */

int num_successes = 3;
int remains; /* number of lines in input file */

char T[MAXN][1+MAX+1][9];
int successes[MAXN] = {0};
char extra = 0; /* extra letter (7+1) */

static int
read_word (FILE *fp, char *s)
{
  int c, n = 0;
  c = fgetc (fp);
  if (c == EOF)
    return 0;
  while ('a' <= c && c <= 'z')
    {
      s[n++] = c;
      c = fgetc (fp);
    }
  assert (c == ' ' || c == '\n' || c == ':');
  ungetc (c, fp);
  s[n] = '\0';
  return n;
}

/* return non-zero if:
   extra = 0 and t has 7 letters
   extra != 0 and t has 7 letters + extra */
static int
compat (char *t)
{
  int n = strlen (t);
  if (extra == 0)
    return n == 7;
  /* now extra != 0 */
  if (n == 7)
    return 0;
  assert (n == 8);
  for (int i = 0; i < n; i++)
    if (t[i] == extra)
      return 1;
  return 0;
}

static int
read_in (char *f)
{
  FILE *fp = fopen (f, "r");
  int ret, c, i, j, remains = 0;
  for (i = 0; i < 120; i++)
    {
      ret = read_word (fp, T[i][0]);
      if (ret == 0)
        break; /* end of file */
      assert (ret == 7);
      int c = fgetc (fp);
      j = 1;
      while (c != '\n' && c != ':')
        {
          assert (c == ' ');
          ret = read_word (fp, T[i][j]);
          assert (ret == 7 || ret == 8);
          assert (j < MAX);
          if (compat (T[i][j]))
            j ++;
          c = fgetc (fp);
        }
      if (c == ':')
        {
          ret = fscanf (fp, "%d", successes + i);
          assert (ret == 1);
          c = fgetc (fp);
          assert (c == '\n');
        }
      else
        successes[i] = 0;
      T[i][j][0] = '\0';
      remains ++;
    }
  fclose (fp);
  printf ("Read %d tirages\n", remains);
  return remains;
}

static int
output (char *f)
{
  FILE *fp = fopen (f, "w");
  int ret, c, i, j, remains_new = 0;
  for (i = 0; i < remains; i++)
    {
      if (successes[i] >= num_successes)
        continue;
      remains_new ++;
      fprintf (fp, "%s", T[i][0]);
      j = 1;
      while (strlen (T[i][j]) != 0)
        {
          fprintf (fp, " %s", T[i][j]);
          j ++;
        }
      if (successes[i] != 0)
        fprintf (fp, ":%d", successes[i]);
      fprintf (fp, "\n");
    }
  fclose (fp);
  return remains_new;
}

unsigned long
isprime (unsigned long n)
{
  unsigned long p;

  if (n == 1)
    return 0;

  if ((n % 2) == 0)
    return n == 2;

  for (p = 3; p * p <= n; p += 2)
    if ((n % p) == 0)
      return 0;

  return 1;
}

static void
permute (char *t, char *s)
{
  unsigned long i, j, n = strlen (s);
  char c;
  
  strcpy (t, s);
  for (i = n-1; i > 0; i--)
    {
      /* permute t[i] with t[0] or t[1] or ... or t[i] */
      j = lrand48 () % i;
      c = t[i];
      t[i] = t[j];
      t[j] = c;
    }
  t[n] = '\0';
}

int
main (int argc, char *argv[])
{
  long seed = 0;
  int k = 0, c, ntries = 50;
  unsigned long P, A;

  while (argc >= 3 && argv[1][0] == '-')
    {
      if (argc >= 3 && strcmp (argv[1], "-seed") == 0)
        {
          seed = strtoul (argv[2], NULL, 10);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 2 && strcmp (argv[1], "-ntries") == 0)
        {
          ntries = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 2 && strcmp (argv[1], "-nsuccesses") == 0)
        {
          num_successes = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 2 && strcmp (argv[1], "-extra") == 0)
        {
          assert (strlen (argv[2]) == 1);
          extra = argv[2][0];
          argc -= 2;
          argv += 2;
        }
      else
        {
          fprintf (stderr, "Unknown option %s\n", argv[1]);
          exit (1);
        }
    }

  assert (argc == 2);
  remains = read_in (argv[1]);

  if (seed == 0)
    seed = getpid ();
  srand48 (seed);
  printf ("Using seed %ld\n", seed);
  /* use X = X + A mod P */
  for (P = remains; !isprime (P); P++);
  long i = lrand48 () % P;
  for (A = lrand48 () % P; A == 0; A = lrand48 () % P);
  for (int j = 0; j < ntries;)
    {
      i = (i + A) % P;
      if (i >= remains)
        continue;
      j ++;
      char t[8];
      permute (t, T[i][0]);
      printf ("%d %s", ++k, t);
      if (extra != 0)
        printf ("+%c", extra);
      printf (" ");
      /* compute the number of anagrams */
      int n;
      for (n = 0; strlen (T[i][n+1]) != 0; n++);
      /* print the number of anagrams if > 3 */
      if (n > 1 || extra != 0)
        printf ("#%d ", n);
      getchar ();
#if 0
      else if (n == 1)
        printf ("%s\n\n", T[i][1]);
      else
        printf ("%s %d\n\n", T[i][1 + (lrand48 () % n)], n);
#else
      printf ("%s", T[i][1]);
      for (int j = 2; j <= n; j++)
        printf (" %s", T[i][j]);
      if (n >= 1)
        printf ("\n");
#endif
      printf ("ok (y/n)? ");
      fflush (stdout);
      c = getchar ();
      if (c != 'n')
        successes[i] ++;
      else
        successes[i] = 0;
#if 0      
      printf ("\n");
      if (c != '\n')
        getchar (); /* to get \n */
#else
      if (c != '\n')
        getchar (); /* to get \n */
      printf ("\n");
#endif      
    }

  char outfile[1024];
  strcpy (outfile, argv[1]);
  strcpy (outfile + strlen (argv[1]), "-tmp");
  int remains_new = output (outfile);
  printf ("Remains %d tirages out of %d\n", remains_new, remains);
  printf ("cp %s %s\n", outfile, argv[1]);
}
