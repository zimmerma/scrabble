/* 31674 mots de 7 lettres
   22271 tirages
16764 tirages avec 1 scrabbles
3423 tirages avec 2 scrabbles
1191 tirages avec 3 scrabbles
458 tirages avec 4 scrabbles
219 tirages avec 5 scrabbles
98 tirages avec 6 scrabbles
57 tirages avec 7 scrabbles
33 tirages avec 8 scrabbles
10 tirages avec 9 scrabbles
4 tirages avec 10 scrabbles
4 tirages avec 11 scrabbles
3 tirages avec 12 scrabbles
3 tirages avec 13 scrabbles
1 tirages avec 14 scrabbles
2 tirages avec 15 scrabbles
1 tirages avec 20 scrabbles */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LEN 7   /* length of words in input file */
#define COUNT 2 /* print the drawings with 2 anagrams */

char T[22271][8];
int n = 0;
int count[22271];
#define MAX_ANAGRAMS 50
int ncount[MAX_ANAGRAMS] = {0,};

static int
is_vowel (char c)
{
  return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y';
}

static int
trie (char *t)
{
  int i, j;
  char c;
  
  for (i = 1; i < strlen (t); i++)
    for (j = i - 1; j >= 0; j--)
      if (t[j] > t[j+1])
        {
          c = t[j];
          t[j] = t[j+1];
          t[j+1] = c;
        }
}

int
main (int argc, char *argv[])
{
  char s[8], t[8], c;
  int ret, i, j, nb_vowels;
  char *search = NULL;
  if (argc >= 3 && strcmp (argv[1], "-search") == 0)
    {
      search = argv[2];
      trie (search);
      argc -= 2;
      argv += 2;
    }
  while ((c = getchar ()) != EOF)
    {
      ungetc (c, stdin);
      ret = scanf ("%s\n", s);
      assert (ret == 1);
      assert (strlen (s) == LEN);
      nb_vowels = 0;
      for (i = 0; i < LEN; i++)
        nb_vowels += is_vowel (s[i]);
#if 0
      /* print the words with 6 vowels */
      if (nb_vowels == 6) printf ("%s\n", s);
#endif      
      strcpy (t, s);
      trie (t);
#if 0      
      for (i = 1; i < LEN; i++)
        for (j = i - 1; j >= 0; j--)
          if (t[j] > t[j+1])
            {
              c = t[j];
              t[j] = t[j+1];
              t[j+1] = c;
            }
#endif      
      if (search != NULL && strcmp (t, search) == 0)
        printf ("%s\n", s);
      for (i = 0; i < n; i++)
        if (strcmp (t, T[i]) == 0)
          break;
      count[i] ++;
      if (i == n)
        {
          assert (n < 22271);
          strcpy (T[n++], t);
        }
      //printf ("%s\n", t);
    }
  assert (n == 22271);
  for (i = 0; i < n; i++)
    {
      assert (count[i] < MAX_ANAGRAMS);
      ncount[count[i]] ++;
      /* print the tirages with 1 scrabble */
      if (count[i] >= COUNT) printf ("%s\n", T[i]);
    }
#if 0  
  for (i = 0; i < MAX_ANAGRAMS; i++)
    if (ncount[i] != 0)
      printf ("%d tirages avec %d scrabbles\n", ncount[i], i);
#endif  
}
