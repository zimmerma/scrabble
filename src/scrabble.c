/* FIXME: when we place only one letter, the two placings (H or V) are
   equivalent (see parties/20180316b) */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>

/* define NONUPLES to count nonuples, for example with -random 3 */
// #define NONUPLES

/* define SCRABBLE=i to count scrabbles at step i, i=1, 2, ... */
// #define SCRABBLE 1

// #define CLASSIQUE

/* if STAT_FILE is defined (it should be a file with 7-letter words),
   computes the number of draws in STAT_FILE (ordering of letter does not
   matter) */
// #define STAT_FILE "lettres_7_2"

/* type de partie */
#define NORMALE 0
#define JOKER78 1 /* 7/8 joker */
#define JOKER 2   /* joker */

/* if interactive is defined, wait a key is pressed before giving
   solutions */
int interactive = 0;

struct dico_struct
{
  char c;
  struct dico_struct *son; /* words starting with letter in c */
  struct dico_struct *brother; /* words starting with another letter */
  int exists; /* tells if word until here exists */
} dico_struct;
typedef struct dico_struct dico_t;

dico_t dico[1];
int nodes = 0;
int nb_coups = 0;
int total = 0, maxi = 0;
/*                 A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S,
                   T, U, V, W, X, Y, Z, ? */
/* in french, there are 9 letters 'A', 2 letters 'B', ... */
int remains[27] = {9, 2, 2, 3,15, 2, 2, 2, 8, 1, 1, 5, 3, 6, 6, 2, 1, 6, 6,
                   6, 6, 2, 1, 1, 1, 1, 2};
/* A corresponds to index 0, ..., Z to index 25, and the joker to index 26 */
int duplicate_words = 0;

/* seed = 0 : letters should be given (default)
   seed = 1 : a random seed is chosen by the program
   seed >= 2 : the random seed is given by the user */
int seed = 0;
int type_partie = NORMALE; /* default is normal */

#define CHAR_JOKER 26

#ifdef STAT_FILE
#define MAX_STAT 16764
unsigned long stat_nwords = 0;
char *stat_words[MAX_STAT][8];
#endif

/* enter the word s of n letters */
static void
enter_word (char *s, int n)
{
  dico_t *d = dico;
  int i;

  for (i = 0; i < n; i++)
    {
      /* first go to the son */
      if (d->son == NULL)
        {
          dico_t *e = malloc (sizeof (dico_t));
          nodes ++;
          d->son = e;
          e->brother = e->son = NULL;
          e->c = s[i];
          e->exists = 0;
          d = e;
        }
      else
        d = d->son;
      /* now search the first letter */
      while (d->c != s[i] && d->brother != NULL)
        d = d->brother;
      /* now d->c = s[i] or d->brother = NULL */
      assert (d->c == s[i] || d->brother == NULL);
      if (d->c != s[i]) /* letter not yet seen at this level */
        {
          dico_t *e = malloc (sizeof (dico_t));
          nodes ++;
          d->brother = e;
          e->brother = e->son = NULL;
          e->c = s[i];
          e->exists = 0;
          d = e;
        }
      /* now d->c = s[i] */
    }
  if (d->exists)
    {
      fprintf (stderr, "Warning, duplicate word %s\n", s);
      duplicate_words ++;
    }
  d->exists = 1;
}

static void
read_dictionary ()
{
  int c, n = 0, nwords = 0;
  char s[15];
  FILE *fp;

  fp = fopen ("dico", "r");

  dico->c = ' ';
  dico->son = dico->brother = NULL;
  dico->exists = 0;

  while ((c = fgetc (fp)) != EOF)
    {
      if ('a' <= c && c <= 'z')
        {
          if (n >= 15)
            {
              fprintf (stderr, "Error, too long word: %s\n", s);
              exit (1);
            }
          s[n++] = c;
        }
      else if (c == '\n')
        {
          s[n] = '\0';
          enter_word (s, n);
          nwords ++;
          n = 0;
        }
      else if (c == '#') /* comment */
        {
          while ((c = fgetc (fp)) != '\n');
        }
      else
        {
          fprintf (stderr, "Error, unexpected character '%c'\n", c);
          exit (1);
        }
    }
  fclose (fp);
  // printf ("Read %d words, allocated %d nodes\n", nwords, nodes);
  if (duplicate_words > 0)
    exit (1);
}

static void
clear (dico_t *d)
{
  if (d != NULL)
    {
      clear (d->son);
      clear (d->brother);
      free (d);
    }
}

static void
clear_dictionary ()
{
  clear (dico->son);
  clear (dico->brother);
}

#ifdef DEBUG
static void
print_node (dico_t *d)
{
  printf ("node %lx: '%c' %d brother=%lx son=%lx\n",
          (long) d, d->c, d->exists, (long) d->brother, (long) d->son);
}

static void
print_dictionary (dico_t *d, char *s0, char *s)
{
  if (d == NULL)
    return;
  /* print sons */
  s[0] = d->c;
  if (d->exists)
    {
      s[1] = '\0';
      printf ("%s\n", s0);
    }
  print_dictionary (d->son, s0, s + 1);
  print_dictionary (d->brother, s0, s);
}
#endif

// a->z : corresponding letter
// $ : mot compte triple
// * : mot compte double
// # : lettre compte triple
// + : lettre compte double
// ' ' : case libre
char tableau[16][16];

#define CASE_SIMPLE ' '
#define LETTRE_COMPTE_DOUBLE '+'
#define	LETTRE_COMPTE_TRIPLE '#'
#define MOT_COMPTE_DOUBLE '*'
#define MOT_COMPTE_TRIPLE '$'

char
valeur_case (int i, int j)
{
  /* 4-fold symmetry */
  if (i > 8)
    i = 16 - i;
  if (j > 8)
    j = 16 - j;
  /* now 1 <= i <= j <= 8 */
  if (i == 8 && j == 8)
    return MOT_COMPTE_DOUBLE; /* case centrale */
  if ((i == 1 || i == 8) && (j == 1 || j == 8))
    return MOT_COMPTE_TRIPLE;
  else if (i == j)
    {
      if (i <= 5)
        return MOT_COMPTE_DOUBLE;
      else if (i == 6)
        return LETTRE_COMPTE_TRIPLE;
      else
        return LETTRE_COMPTE_DOUBLE;
    }
  else if ((i == 2 || i == 6) && (j == 2 || j == 6))
    return LETTRE_COMPTE_TRIPLE;
  else if ((i == 1 || i == 4) && (j == 1 || j == 4))
    return LETTRE_COMPTE_DOUBLE;
  else if ((i == 4 || i == 8) && (j == 4 || j == 8))
    return LETTRE_COMPTE_DOUBLE;
  else if ((i == 3 || i == 7) && (j == 3 || j == 7))
    return LETTRE_COMPTE_DOUBLE;
  else
    return CASE_SIMPLE;
}

static int
valeur_lettre (char c)
{
  /* a joker is represented by a..z, normal letters by A..Z */
  assert (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'));
  if (islower (c)) /* joker */
    return 0;
  c = tolower (c);
  if (c == 'k' || c == 'w' || c == 'x' || c == 'y' || c == 'z')
    return 10;
  if (c == 'j' || c == 'q')
    return 8;
  if (c == 'f' || c == 'h' || c == 'v')
    return 4;
  if (c == 'b' || c == 'c' || c == 'p')
    return 3;
  if (c == 'd' || c == 'g' || c == 'm')
    return 2;
  return 1;
}

/* return the point when letter (i,j) was put, and updates multiplier */
static int
update_score (int i, int j, int *multiplier)
{
  if (valeur_case (i, j) == MOT_COMPTE_DOUBLE)
    *multiplier *= 2;
  if (valeur_case (i, j) == MOT_COMPTE_TRIPLE)
    *multiplier *= 3;
  if (valeur_case (i, j) == LETTRE_COMPTE_TRIPLE)
    return 3 * valeur_lettre (tableau[i][j]);
  else if (valeur_case (i, j) == LETTRE_COMPTE_DOUBLE)
    return 2 * valeur_lettre (tableau[i][j]);
  else
    return valeur_lettre (tableau[i][j]);
}

static int
downdate_score (int i, int j, int *multiplier)
{
  if (valeur_case (i, j) == MOT_COMPTE_DOUBLE)
    *multiplier /= 2;
  if (valeur_case (i, j) == MOT_COMPTE_TRIPLE)
    *multiplier /= 3;
  if (valeur_case (i, j) == LETTRE_COMPTE_TRIPLE)
    return 3 * valeur_lettre (tableau[i][j]);
  else if (valeur_case (i, j) == LETTRE_COMPTE_DOUBLE)
    return 2 * valeur_lettre (tableau[i][j]);
  else
    return valeur_lettre (tableau[i][j]);
}

static void
make_tableau ()
{
  int i, j;

  for (i = 1; i <= 15; i++)
    for (j = 1; j <= 15; j++)
      tableau[i][j] = ' '; /* case libre */
}

static void
print_tableau ()
{
  int i, j;
  printf ("  1 2 3 4 5 6 7 8 9101112131415\n");
  printf (" -------------------------------\n");
  for (j = 1; j <= 15; j++)
    {
      printf ("%c|", 'A' + (j - 1));
      for (i = 1; i <= 15; i++)
        if (tableau[i][j] == ' ')
          printf ("%c|", valeur_case (i, j));
        else
          printf ("%c|", tableau[i][j]);
      if (j == 11)
        printf ("      $ : mot compte triple");
      else if (j == 9)
        printf ("      * : mot compte double");
      else if (j == 7)
        printf ("      # : lettre compte triple");
      else if (j == 5)
        printf ("      + : lettre compte double");
      printf ("\n");
    }
  printf (" -------------------------------\n");
}

#define HORIZONTAL 0
#define VERTICAL 1

/* compare letter 'c' in A..Z with tableau[i][j], in A..Z or a..z (joker) */
int same_letter (char c, int i, int j)
{
  /* the input dictionary is in lowercase, thus c is in a..z */
  assert ('a' <= c && c <= 'z');
  return c == tolower (tableau[i][j]);
}

#define MAX_SCRABBLES 64
int Nbest = 20;
char best_word[16];  /* best word found so far */
int nb_maxscore = 0; /* number of words with best score */
char Best_word[MAX_SCRABBLES][16]; /* first, second, third, ... best words */
int Best_score[MAX_SCRABBLES];       /* scores of Best_word[i] */
char Best_position[MAX_SCRABBLES][4];
int nbest = 0; /* number of best scores registered */
int maxscore, besti, bestj, bestdir;
int verbose = 0;
int nb_scrabbles = 0; /* number of different words giving a scrabble */
char scrabbles[MAX_SCRABBLES][16];
char position_scrabble[MAX_SCRABBLES][4];
int score_scrabble[MAX_SCRABBLES];

/* put in s[] the position (and adds a terminating \0) */
void
get_position (char *s, int i, int j, int dir)
{
  if (dir == HORIZONTAL)
    sprintf (s, "%c%d", 'A' + (j - 1), i);
  else
    sprintf (s, "%d%c", i, 'A' + (j - 1));
}

static void
update_Best (int i, int j, int dir, int score, char *current_word)
{
  if (nbest == MAX_SCRABBLES && score <= Best_score[nbest-1])
    return;
  /* now either nbest < MAX_SCRABBLES or score > Best_score[nbest-1] */
  int k;
  for (k = nbest; k > 0 && score > Best_score[k-1]; k--)
    {
      if (k < MAX_SCRABBLES)
        {
          Best_score[k] = Best_score[k-1];
          strncpy (Best_word[k], Best_word[k-1], 16);
          strncpy (Best_position[k], Best_position[k-1], 4);
        }
    }
  assert (k < MAX_SCRABBLES);
  Best_score[k] = score;
  strncpy (Best_word[k], current_word, 16);
  get_position (Best_position[k], i, j, dir);
  if (nbest < MAX_SCRABBLES)
    nbest ++;
}

static void
update_best_score (int i, int j, int dir, int score, char *current_word,
                   int used_letters)
{
  if (used_letters == 7)
    {
      score += 50;
      /* check if new word */
      int k, min_score = 999, min_k = -1;
      for (k = 0; k < nb_scrabbles; k++)
      {
        if (strcmp (current_word, scrabbles[k]) == 0)
          break; /* already seen */
        if (score_scrabble[k] < min_score)
        {
          min_score = score_scrabble[k];
          min_k = k;
        }
      }
      /* if we already have MAX_SCRABBLES scrabbles, but we have a new one with
         larger score than the minimal one, replace the smallest one */
      if (k == nb_scrabbles && nb_scrabbles == MAX_SCRABBLES
          && min_score < score)
        k = min_k;
      /* when we have already seen a scrabble, we replace it if the new
         position has a better score */
      if ((k == nb_scrabbles && nb_scrabbles < MAX_SCRABBLES) ||
          (k < nb_scrabbles && score > score_scrabble[k]))
        {
          strncpy (scrabbles[k], current_word, 16);
          get_position (position_scrabble[k], i, j, dir);
          score_scrabble[k] = score;
          nb_scrabbles += k == nb_scrabbles;
        }
    }

  /* update the best scores */
  update_Best (i, j, dir, score, current_word);

  /* if score = maxscore, we change the best word with probability
     1/(nb_maxscore + 1), which will give uniform probability */
  if (score > maxscore ||
      (score == maxscore && (lrand48 () % (nb_maxscore + 1)) == 0))
    {
      if (score > maxscore)
        nb_maxscore = 0;
      nb_maxscore ++;
      maxscore = score;
      besti = i;
      bestj = j;
      bestdir = dir;
      assert (strlen (current_word) <= 15);
      strncpy (best_word, current_word, 16);
      if (verbose > 0)
        {
          char position[4];
          get_position (position, besti, bestj, bestdir);
          printf ("   %s en %s pour %d points\n",
                  best_word, position, maxscore);
        }
    }
}

/* return -1 if c is not in tirage,
   otherwise return the 1st position of c in tirage (not a joker)
   assume tirage ends with a '\0'
*/
static int
is_in_tirage (char c, char *tirage)
{
  size_t i;
  c = toupper (c);
  for (i = 0; i < strlen (tirage); i++)
    if (tirage[i] == c)
      return i;
  return -1;
}

/* return -1 if there is no joker in tirage,
   otherwise return the 1st position of a joker in tirage
   assume tirage ends with a '\0'
*/
static int
joker_in_tirage (char *tirage)
{
  size_t i;
  for (i = 0; i < strlen (tirage); i++)
    if (tirage[i] == '?')
      return i;
  return -1;
}

struct raccord_struct
{
  int valid; /* 1: form at least a 2-letter word with
                   existing letters (which might count 0 if two jokers)
                0: no valid raccord so far
                -1: invalid raccord */
  int score; /* total numbers of points of the raccords */
} raccord_struct;
typedef struct raccord_struct raccord_t[1];

/* dir is the direction of the raccord (orthogonal to the placed word)
   Set r->valid to 0 if the raccord is not valid, otherwise set r->valid to 1
   and add in r->score the raccord score (which is 0 if there is only one
   letter or a word with 2 jokers)
   (i,j) is the cell of the new word
   Assume r->valid is 0 or 1 at input. */
static void
valid_raccord (raccord_t r, int i, int j, int dir)
{
  int di = 1 - dir, dj = dir, score = 0, multiplier = 1, len = 0;
  int letter_before, letter_after;

  if (nb_coups == 0)
    return; /* no raccord needed at first draw */

  letter_before = i - di >= 1 && j - dj >= 1 && tableau[i-di][j-dj] != ' ';
  letter_after = i + di <= 15 && j + dj <= 15 && tableau[i+di][j+dj] != ' ';

  if (letter_before == 0 && letter_after == 0)
    return; /* one letter is ok, but gives no point, don't change valid */

  // printf ("enter valid_raccord i=%d j=%d dir=%d\n", i, j, dir);

  if (valeur_case (i, j) == LETTRE_COMPTE_DOUBLE)
    score += valeur_lettre (tableau[i][j]); /* will add valeur below */
  else if (valeur_case (i, j) == LETTRE_COMPTE_TRIPLE)
    score += 2 * valeur_lettre (tableau[i][j]); /* will add valeur below */
  else if (valeur_case (i, j) == MOT_COMPTE_DOUBLE)
    multiplier = 2;
  else if (valeur_case (i, j) == MOT_COMPTE_TRIPLE)
    multiplier = 3;

  if (dir == HORIZONTAL) /* raccord is horizontal */
    while (i > 1 && tableau[i-1][j] != ' ')
      i--;
  else /* raccord is vertical */
    while (j > 1 && tableau[i][j-1] != ' ')
      j--;

  /* now check if the word starting at (i,j) with direction dir is valid */
  dico_t *d = dico;
  while (i <= 15 && j <= 15 && tableau[i][j] != ' ')
    {
      if (d->son == NULL)
        {
          // printf ("valid_raccord: no more word\n");
          r->valid = -1;
          return; /* no more word */
        }
      d = d->son;
      /* check for letter at i,j */
      while (same_letter (d->c, i, j) == 0 && d->brother != NULL)
        d = d->brother;
      assert (same_letter (d->c, i, j) || d->brother == NULL);
      if (same_letter (d->c, i, j) == 0)
        {
          r->valid = -1;
          return;
        }
      /* count points of current letter */
      score += valeur_lettre (tableau[i][j]);
      i = i + di, j = j + dj;
      len ++;
    }
  if (d->exists == 0)
    r->valid = -1;
  else
  {
    r->valid = 1;
    r->score += multiplier * score;
  }
}

/* tirage is the remaining letters to be placed, starting from (i,j) with
   direction dir, with cell (i,j) not yet filled
   d is the current node in the dictionary
   multiplier is the current multiplier (1, 2, 3, 6 or 9)
   raccord_score is the current state of the "raccord" words:
      raccord_score->valid > 0 if there is at least a valid raccord
      (and raccord_score->valid = 0 if there is so far no invalid raccord)
      raccord_score->score is the total score of current raccords
   score is the current score of the word in direction dir
   current_word is the current word (not counting cell (i,j))
   len is the length of the current word (not counting cell (i,j))
   used_letters is the number of letters used from the tirage
   return the maximal total score
 */
static void
search_aux2 (char *tirage, int i, int j, int dir, dico_t *d, int multiplier,
            raccord_t raccord_score, int score, char *current_word, size_t len,
            size_t used_letters)
{
  int di = 1 - dir, dj = dir;

  assert (len == strlen (current_word));

  /* check if previous word is valid (if no letter after).
     On the first tirage, the word should cover the central cell, i.e., the
     next cell should have i > 8.
     On subsequent tirages, we should join the current letters, which means:
     (a) either raccord_score->valid > 0
     (b) or len > used_letters */
  if (used_letters > 0 && d->exists &&
      (i == 16 || j == 16 || tableau[i][j] == ' ') &&
      ((nb_coups == 0 && i > 8) ||
       (nb_coups > 0 && (raccord_score->valid > 0 || len > used_letters))))
    update_best_score (i - len * di, j - len * dj, dir,
                       multiplier * score + raccord_score->score, current_word,
                       used_letters);

  if (i == 16 || j == 16) /* no more valid cell */
    {
      // printf ("end of the board\n");
      return;
    }

  assert (i <= 15 && j <= 15);

  if (d->son == NULL) /* no more valid word */
    {
      // printf ("no more valid word\n");
      return;
    }
  d = d->son; /* go to next level of the tree */

  if (tableau[i][j] != ' ') /* cell (i,j) already filled */
    {
      // printf ("cell (%d,%d) already filled\n", i, j);
      while (same_letter (d->c, i, j) == 0 && d->brother != NULL)
        d = d->brother;
      /* now d->c = tableau[i][j] or d->brother = NULL */
      assert (same_letter (d->c, i, j) || d->brother == NULL);
      if (same_letter (d->c, i, j) == 0) /* no word with this letter */
        return;
      current_word[len++] = tableau[i][j];
      current_word[len] = '\0';
      if (dir == HORIZONTAL)
        search_aux2 (tirage, i + 1, j, dir, d, multiplier, raccord_score,
                    score + valeur_lettre (tableau[i][j]), current_word,
                    len, used_letters);
      else
        search_aux2 (tirage, i, j + 1, dir, d, multiplier, raccord_score,
                    score + valeur_lettre (tableau[i][j]), current_word,
                    len, used_letters);
      current_word[--len] = '\0';
    }
  else /* cell (i,j) not yet filled */
    {
      /* for 7/8 joker, we place only up to 7 letters */
      if (type_partie == JOKER78 && used_letters == 7)
        return;

      while (d != NULL)
        {
          int k, n = strlen (tirage);
          char saved_letter;
          /* look if d->c is in tirage */
          k = is_in_tirage (d->c, tirage);
          raccord_t saved;
          saved->valid = raccord_score->valid;
          saved->score = raccord_score->score;
          if (k >= 0)
            {
              /* place letter on the board */
              tableau[i][j] = toupper (d->c);
              /* remove letter from tirage */
              saved_letter = tirage[k];
              tirage[k] = tirage[n-1];
              tirage[n-1] = '\0';
              /* check if raccord is valid */
              valid_raccord (raccord_score, i, j, 1 - dir);
              if (raccord_score->valid >= 0)
                {
                  score += update_score (i, j, &multiplier);
                  current_word[len++] = toupper (d->c);
                  current_word[len] = '\0';
                  search_aux2 (tirage, i + di, j + dj, dir, d, multiplier,
                              raccord_score, score, current_word, len,
                              used_letters + 1);
                  current_word[--len] = '\0';
                  score -= downdate_score (i, j, &multiplier);
                }
              /* restore raccord_score */
              raccord_score->valid = saved->valid;
              raccord_score->score = saved->score;
              /* put back letter in tirage at previous place */
              tirage[n-1] = tirage[k];
              assert (tirage[n] == '\0');
              tirage[k] = saved_letter;
              /* remove letter from the board */
              tableau[i][j] = ' ';
            }
          /* look if there is a joker that one could use for d->c */
          k = joker_in_tirage (tirage);
          if (k >= 0)
            {
              /* place letter on the board */
              tableau[i][j] = tolower (d->c); /* to remember it is a joker */
              /* remove letter from tirage */
              saved_letter = tirage[k];
              tirage[k] = tirage[n-1];
              tirage[n-1] = '\0';
              /* check if raccord is valid */
              valid_raccord (raccord_score, i, j, 1 - dir);
              if (raccord_score->valid >= 0)
                {
                  score += update_score (i, j, &multiplier);
                  current_word[len++] = tolower (d->c);
                  current_word[len] = '\0';
                  search_aux2 (tirage, i + di, j + dj, dir, d, multiplier,
                              raccord_score, score, current_word, len,
                              used_letters + 1);
                  current_word[--len] = '\0';
                  score -= downdate_score (i, j, &multiplier);
                }
              /* restore raccord_score */
              raccord_score->valid = saved->valid;
              raccord_score->score = saved->score;
              /* put back joker in tirage at previous place */
              tirage[n-1] = tirage[k];
              assert (tirage[n] == '\0');
              tirage[k] = saved_letter;
              /* remove letter from the board */
              tableau[i][j] = ' ';
            }
          d = d->brother;
        }
    }
}

/* try to make a word starting at cell (i,j), with direction 'dir',
   and return the best score */
static void
search_aux (char *tirage, int i, int j, int dir)
{
#if 0
  if (i == 15 && j == 8 && dir == VERTICAL && nb_coups == 5)
    printf ("enter search_aux, i=%d, j=%d, dir=%d, tirage='%s'\n",
            i, j, dir, tirage);
#endif
  /* if the previous cell is already filled, this cannot work */
  if (dir == HORIZONTAL && i > 1 && tableau[i-1][j] != ' ')
    return;
  if (dir == VERTICAL && j > 1 && tableau[i][j-1] != ' ')
    return;

  char current_word[15];
  current_word[0] = '\0';
  raccord_t r;
  r->valid = 0;
  r->score = 0;
  search_aux2 (tirage, i, j, dir, dico, 1, r, 0, current_word, 0, 0);
}

static void
search (char *tirage, int print_best)
{
  int n = strlen (tirage), i, j;

  /* the first word is horizontal, and starts at (i,8) with 2 <= i <= 8 */

  // printf ("enter search, tirage='%s'\n", tirage);

  if (type_partie == JOKER78 && n == 8)
    n = 7;

  for (i = 1; i <= 15; i++)
    {
      for (j = 1; j <= 15; j++)
        {
          if (nb_coups == 0 && (i + n <= 8 || 8 < i || j != 8))
            continue;
          search_aux (tirage, i, j, HORIZONTAL);
          if (nb_coups > 0) /* the first word is horizontal */
            search_aux (tirage, i, j, VERTICAL);
        }
    }

  if (verbose >= 0 && print_best && maxscore > 0)
    {
      char position[4];
      get_position (position, besti, bestj, bestdir);

      printf ("top: %s %s pour %d points\n", best_word, position, maxscore);
      for (int k = 0; k < nbest && k < Nbest; k++)
        {
          printf ("\t%s %s %d", Best_word[k], Best_position[k], Best_score[k]);
          if ((k % 4) == 3) printf ("\n");
        }
      if ((nbest % 4) != 0) printf ("\n");
      if (nb_scrabbles > 0)
        {
          int j, k;
          printf ("nombre de scrabble(s) possible(s) : %d\n", nb_scrabbles);
          /* sort scrabbles */
          for (j = 1; j < nb_scrabbles; j++)
            for (k = j - 1; k >= 0; k--)
              if (score_scrabble[k] < score_scrabble[k+1])
                {
                  char s[16];
                  int tmp;
                  strcpy (s, scrabbles[k]);
                  strcpy (scrabbles[k], scrabbles[k+1]);
                  strcpy (scrabbles[k+1], s);
                  strcpy (s, position_scrabble[k]);
                  strcpy (position_scrabble[k], position_scrabble[k+1]);
                  strcpy (position_scrabble[k+1], s);
                  tmp = score_scrabble[k];
                  score_scrabble[k] = score_scrabble[k+1];
                  score_scrabble[k+1] = tmp;
                }
          for (k = 0; k < nb_scrabbles && k < 15; k++)
            printf ("   %s %s pour %d points\n", scrabbles[k],
                    position_scrabble[k], score_scrabble[k]);
        }
    }
}

/* should be called only for the chosen word, not on the tirage */
static int
is_joker (char c)
{
  return islower (c);
}

/* remove the letter c from tirage */
static void
retire (char *c, char *tirage)
{
  int i, n = strlen (tirage), is_jok = 0;

  if (is_joker (*c))
    {
      i = joker_in_tirage (tirage);
      is_jok = 1;
    }
  else
    i = is_in_tirage (*c, tirage);
  assert (i >= 0);
  /* for joker and 7/8 joker, if the letter remains, we use it instead of the
     joker */
  if ((type_partie == JOKER || type_partie == JOKER78) && is_jok
      && remains[*c - 'a'] > 0)
    {
      remains[*c - 'a'] --;
      remains[CHAR_JOKER] ++;
      *c = toupper (*c);
    }
  tirage[i] = tirage[n-1];
  tirage[n-1] = '\0';
}

/* put back letters from tirage in the urn */
static void
remet_lettres (char *tirage)
{
  int i, n = strlen (tirage);
  for (i = 0; i < n; i++)
    {
      int c = tirage[i];
      if (c == '?')
        c = CHAR_JOKER;
      else
        c = tolower (c) - 'a';
      remains[c] ++;
    }
}

/* also removes the letters from tirage */
static int
compte_points (char *tirage)
{
  int multiplier = 1, score = 0;
  int i = besti, j = bestj, k, n = strlen (best_word);
  int di = 1 - bestdir, dj = bestdir, used_letters = 0;
  raccord_t raccord_score;
  raccord_score->score = 0;

  for (k = 0; k < n; k++, i+=di, j+=dj)
    if (tableau[i][j] == ' ')
      {
        tableau[i][j] = best_word[k];
        score += update_score (i, j, &multiplier);
        valid_raccord (raccord_score, i, j, 1 - bestdir);
        tableau[i][j] = ' ';
        used_letters ++;
        retire (&(best_word[k]), tirage);
      }
    else
      score += valeur_lettre (tableau[i][j]);
  return multiplier * score + raccord_score->score + 50 * (used_letters == 7);
}

static int
remaining_jokers ()
{
  return remains[CHAR_JOKER];
}

static void
place_best_word (char *tirage)
{
  int i = besti, j = bestj, k, n = strlen (best_word);
  int di = 1 - bestdir, dj = bestdir, score;
  char position[4];

  score = compte_points (tirage); /* also remove used letters */
  total += score;
  maxi += score;
  get_position (position, besti, bestj, bestdir);
  if (verbose >= 0)
    printf ("on place %s %s pour %d points, total %d, maxi %d\n",
            best_word, position, score, total, maxi);
  else
    printf (" %s %s %d\n", best_word, position, maxscore);
  /* best_word[k] is in A..Z for normal letter, in a..z for jokers */
  for (k = 0; k < n; k++)
    tableau[i + k * di][j + k * dj] = best_word[k];
  maxscore = nb_maxscore = nb_scrabbles = nbest = 0;
  remet_lettres (tirage);
}

static int
remaining_letters ()
{
  int s = 0, i;

  for (i = 0; i < 27; i++)
    s += remains[i];
  return s;
}

/* we do not consider Y as vowel here */
static int
remaining_vowels ()
{
  char vowels[] = "AEIOU", c;
  int s = 0, i;

  for (i = 0; i < 5; i++)
    {
      c = vowels[i];
      s += remains[c - 'A'];
    }
  return s;
}

static int
remaining_vowels_with_y ()
{
  char vowels[] = "AEIOUY", c;
  int s = 0, i;

  for (i = 0; i < 6; i++)
    {
      c = vowels[i];
      s += remains[c - 'A'];
    }
  return s;
}

static int
remaining_consonants ()
{
  char consonants[] = "BCDFGHJKLMNPQRSTVWXZ", c;
  int s = 0, i;

  for (i = 0; i < 20; i++)
    {
      c = consonants[i];
      s += remains[c - 'A'];
    }
  return s;
}

static int
remaining_consonants_with_y ()
{
  char consonants[] = "BCDFGHJKLMNPQRSTVWXZY", c;
  int s = 0, i;

  for (i = 0; i < 21; i++)
    {
      c = consonants[i];
      s += remains[c - 'A'];
    }
  return s;
}

static int
tirage_is_valid_joker78 (char *tirage)
{
  int i, n = strlen (tirage);
  int vowels = 0; /* number of vowels */
  int consonants = 0; /* number of consonants */
  int ret = 1;
  int target = 7 + (type_partie == JOKER78);

  if (target > remaining_letters ())
    target = remaining_letters () ;
  if (n > target)
    {
      printf ("Erreur, trop de lettres dans le tirage\n");
      return 0;
    }
  if (n < target)
    {
      printf ("Erreur, pas assez de lettres dans le tirage: %s\n", tirage);
      return 0;
    }
  for (i = 0; i < n; i++)
    {
      char c = toupper (tirage[i]);
      int j = c == '?' ? CHAR_JOKER : c - 'A';
      int is_vowel = c == 'A' || c == 'E' || c == 'I' ||
        c == 'O' || c == 'U';
      vowels += is_vowel;
      consonants += (is_vowel || c == 'Y' || c == '?') == 0;
      if (remains[j] == 0)
        {
          printf ("Erreur, le tirage contient plus de lettres %c qu'il en reste dans le sac: %s\n", c, tirage);
          ret = 0;
        }
      remains[j] --;
    }
  remet_lettres (tirage);
  int remain_vowels = remaining_vowels ();
  int remain_consonants = remaining_consonants ();
  int vowels_bound, consonants_bound;
  if (nb_coups < 15)
    vowels_bound = consonants_bound = 2;
  else
    vowels_bound = consonants_bound = 1;
  vowels_bound = (remain_vowels < vowels_bound) ? remain_vowels : vowels_bound;
  consonants_bound = (remain_consonants < consonants_bound)
    ? remain_consonants : consonants_bound;
  if (vowels < vowels_bound || consonants < consonants_bound)
    {
      if (verbose >= 0)
        {
          printf ("Erreur, le tirage ne contient pas assez de voyelles ou de consonnes: %s\n", tirage);
          printf ("Coup %d: reste %d consonne(s), %d voyelle(s)\n", nb_coups + 1,
                  remaining_consonants (), remaining_vowels ());
        }
      ret = 0;
    }
  return ret;
}

/* if error is 1, print errors */
static int
tirage_is_valid (char *tirage, int error)
{
  int i, n = strlen (tirage);
  int vowels = 0; /* number of vowels */
  int consonants = 0; /* number of consonants */
  int ret = 1;

  if (type_partie == JOKER78)
    return tirage_is_valid_joker78 (tirage);

  if (n > 7)
    {
      printf ("Erreur, trop de lettres dans le tirage\n");
      return 0;
    }
#ifndef CLASSIQUE /* if 0 pour la fin du jeu en classique */
  if (n < 7 && remaining_letters () > n)
    {
      printf ("Erreur, pas assez de lettres dans le tirage: %s\n", tirage);
      return 0;
    }
#endif
  for (i = 0; i < n; i++)
    {
      char c = toupper (tirage[i]);
      int j = c == '?' ? CHAR_JOKER : c - 'A';
      int is_vowel = c == 'A' || c == 'E' || c == 'I' ||
        c == 'O' || c == 'U';
      vowels += is_vowel;
      consonants += (is_vowel || c == 'Y' || c == '?') == 0;
      /* if game is given by the user (seed=0) then jokers might be interpreted
         as vowels or consonants */
      if (seed == 0)
        {
          vowels += c == '?';
          consonants += c == '?';
        }
      if (remains[j] == 0)
        {
          printf ("Erreur, le tirage contient plus de lettres %c qu'il en reste dans le sac: %s\n", c, tirage);
          ret = 0;
        }
      remains[j] --;
    }
  remet_lettres (tirage);
  int remain_vowels = remaining_vowels ();
  int remain_consonants = remaining_consonants ();
  int vowels_bound, consonants_bound;
  if (nb_coups < 15)
    vowels_bound = consonants_bound = 2;
  else
    vowels_bound = consonants_bound = 1;
  vowels_bound = (remain_vowels < vowels_bound) ? remain_vowels : vowels_bound;
  consonants_bound = (remain_consonants < consonants_bound)
    ? remain_consonants : consonants_bound;
  if (vowels < vowels_bound || consonants < consonants_bound)
    {
      if (error)
        {
          printf ("Erreur, le tirage ne contient pas assez de voyelles ou de consonnes: %s\n", tirage);
          printf ("Coup %d: reste %d consonne(s), %d voyelle(s)\n", nb_coups + 1,
                  remaining_consonants (), remaining_vowels ());
        }
      ret = 0;
    }
  return ret;
}

static void
convert_to_upper (char *tirage)
{
  int i, n = strlen (tirage);
  for (i = 0; i < n; i++)
    tirage[i] = toupper (tirage[i]); /* convert to uppercase */
}

static void
sort_tirage (char *s)
{
  int i, j, n = strlen (s);
  for (i = 1; i < n; i++)
    {
      j = i;
      char c = s[j];
      while (j > 0 && c < s[j-1])
        {
          s[j] = s[j-1];
          j--;
        }
      s[j] = c;
    }
}

static void
retire_lettres (char *tirage)
{
  int i, n = strlen (tirage);

  for (i = 0; i < n; i++)
    {
      int c = tirage[i];
      if (c == '?')
        c = CHAR_JOKER;
      else
        {
          c = tolower (c);
          assert ('a' <= c && c <= 'z');
          c = c - 'a';
        }
      assert (remains[c] > 0);
      remains[c] --;
    }
}

/* we are done when there is no more vowel or consonant, where Y and ? can be
   considered as vowel or consonant */
static int
end_of_game ()
{
  return (remaining_vowels_with_y () == 0 ||
          remaining_consonants_with_y () == 0) && remaining_jokers () == 0;
}

#define BUF_SIZE 1024

/* removes + and initial - */
static void
purge_tirage (char *tirage, const char *tirage0)
{
  int n = strlen (tirage0), i, j;

  for (i = j = 0; i < n; i++)
    {
      if (i == 0 && tirage0[i] == '-') /* rejet */
        {
        }
      else if (tirage0[i] == '+') /* reliquat */
        {
        }
      else
        tirage[j++] = tirage0[i];
    }
  tirage[j] = '\0';
}

/* Return 0 in case of error.
   tirage0 is the initial tirage (with initial '-' and '+' if any)
   tirage  is the purged one */
static int
lit_tirage (char *tirage0, char *tirage, char *motchoisi, char *place)
{
  char line[BUF_SIZE];
  int ret = 1;

  if (seed == 0) /* letters are given by user */
    {
      /* line format is either: tirage motchoisi place
         or: tirage */
      while (1)
        {
          if (fgets (line, BUF_SIZE, stdin) == NULL)
            return 0;
          if (line[0] == '#') /* ignore lines starting with # */
            continue;
          ret = sscanf (line, "%s %s %s\n", tirage0, motchoisi, place);
          purge_tirage (tirage, tirage0);
          if (ret == 1 || ret == 3)
            {
              if (tirage_is_valid (tirage, 1) == 0)
                {
                  printf ("Entrez à nouveau le tirage.\n");
                  continue;
                }
              else
                break;
            }
        }
      convert_to_upper (tirage); /* convert to upper case */
      retire_lettres (tirage);
    }
  else /* random game */
    {
      int rem_letters = remaining_letters (); /* including those in tirage */
      int n;
      n = (rem_letters >= 7) ? 7 : rem_letters;
      if (type_partie == JOKER78)
        n = (rem_letters >= 8) ? 8 : n;

      /* we decrease the count of letters in tirage */
      retire_lettres (tirage);
      rem_letters -= strlen (tirage);
      if (verbose >= 0 && strlen (tirage) > 0)
        printf ("Il reste %s\n", tirage);
      if (interactive)
        getchar ();

      /* we draw n-strlen(tirage) random letters */
      int reliquat, rejet = 0;
      while (1)
        {
          reliquat = strlen (tirage);
          int i = reliquat; /* number of already drawn letters */
          int vowels = 0; /* number of vowels */
          int consonants = 0; /* number of consonants */
          char c;

          /* for joker and 7/8 joker, if no joker, we draw one if remaining */
          if ((type_partie == JOKER || type_partie == JOKER78) &&
              joker_in_tirage (tirage) == -1 && remaining_jokers () > 0)
            {
              remains[CHAR_JOKER] --;
              rem_letters --;
              tirage[i++] = '?';
            }

          while (i < n)
            {
              int rnd, k;
              /* for joker and 7/8 joker, we draw only one joker at a time */
              do {
                rnd = lrand48 () % rem_letters;
                k = 0;
                for (k = 0; rnd >= remains[k]; k++)
                  rnd -= remains[k];
              } while ((type_partie == JOKER || type_partie == JOKER78) &&
                       k == JOKER && rem_letters > remains[CHAR_JOKER]);
              /* now 0 <= rnd < remains[k] which implies remains[k] > 0 */
              assert (0 <= k && k <= CHAR_JOKER);
              remains[k] --;
              rem_letters --;
              c = (k < 26) ? 'A' + k : '?';
              tirage[i++] = c;
              int is_vowel = c == 'A' || c == 'E' || c == 'I' ||
                c == 'O' || c == 'U';
              vowels += is_vowel;
              consonants += (is_vowel || c == 'Y' || c == '?') == 0;
            }
          tirage[n] = '\0';
          /* put back letters */
          remet_lettres (tirage);
          rem_letters += strlen (tirage);
          if (tirage_is_valid (tirage, 0))
            {
              retire_lettres (tirage);
              rem_letters -= strlen (tirage);
              break;
            }
          else
            {
              tirage[0] = '\0';
              rejet = 1;
            }
        }
      if (rejet)
        {
          tirage0[0] = '-';
          tirage0 ++;
        }
      if (reliquat == 0)
        strcpy (tirage0, tirage);
      else
        {
          strncpy (tirage0, tirage, reliquat);
          tirage0[reliquat] = '\0';
          sort_tirage (tirage0);
          tirage0[reliquat] = '+';
          strcpy (tirage0 + reliquat + 1, tirage + reliquat);
        }
    }
  return ret;
}

#ifdef STAT_FILE
static void
convert_to_lower (char *tirage)
{
  int i, n = strlen (tirage);
  for (i = 0; i < n; i++)
    tirage[i] = tolower (tirage[i]); /* convert to lowercase */
}

static void
stat_find (char *tirage)
{
  char t[8];
  strcpy (t, tirage);
  convert_to_lower (t);
  sort_tirage (t);
  for (unsigned long i = 0; i < stat_nwords; i++)
    if (strcmp (t, (char*) stat_words[i]) == 0)
      {
        printf ("STAT: %s\n", tirage);
        break;
      }
}
#endif

static void
joue ()
{
  char tirage[16], tirage0[18], motchoisi[16], place[4], c;
  int ret;

  if (seed > 0) /* random game */
    {
      if (seed == 1)
        {
          seed = getpid ();
          assert (seed > 0);
        }
      printf ("Partie %d\n", seed);
      srand48 (seed);
    }

  tirage[0] = '\0';
  while (end_of_game () == 0)
    {
      ret = lit_tirage (tirage0, tirage, motchoisi, place);
      if (ret == 0)
        break; /* end of game */
      if (verbose >= 0)
        printf ("Coup %d: tirage %s\n", nb_coups + 1, tirage0);
      else
        printf ("%d %s", nb_coups + 1, tirage0);
      if (interactive)
        getchar ();
#ifdef STAT_FILE
      stat_find (tirage);
#endif
      search (tirage, 1);
      if (maxscore == 0) /* no word is possible */
        {
          static int failures = 0;
          printf ("aucun mot possible, on rejette\n");
          remet_lettres (tirage);
          if (++failures >= 10)
            {
              printf ("10 tirages sans aucun mot possible, on termine\n");
              exit (1);
            }
          continue;
        }
      //      total += maxscore;
      if (ret == 3) /* given solution */
        {
          assert (strlen (motchoisi) <= 15);
          strncpy (best_word, motchoisi, 16);
          if (isdigit (place[0]))
            {
              ret = sscanf (place, "%d%c", &besti, &c);
              assert (ret == 2);
              c = toupper (c);
              assert ('A' <= c && c <= 'O');
              bestj = c - 'A' + 1;
              bestdir = VERTICAL;
            }
          else
            {
              ret = sscanf (place, "%c%d", &c, &besti);
              assert (ret == 2);
              c = toupper (c);
              assert ('A' <= c && c <= 'O');
              bestj = c - 'A' + 1;
              bestdir = HORIZONTAL;
            }
        }
      else
        assert (ret == 1);
      if (interactive)
        getchar ();
      place_best_word (tirage);
      if (verbose >= 0)
        print_tableau ();
      nb_coups ++;
    }
  if (verbose < 0)
    printf ("Total %d\n", total);
}

#ifdef STAT_FILE
static void
read_stat_file ()
{
  FILE *stat_file = fopen (STAT_FILE, "r");
  char s[1024];
  while (! feof (stat_file))
    {
      int ret = fscanf (stat_file, "%s\n", s);
      if (ret == EOF)
        break;
      assert (ret == 1);
      assert (strlen (s) == 7);
      convert_to_lower (s);
      sort_tirage (s);
      assert (stat_nwords < MAX_STAT);
      strcpy ((char*) stat_words[stat_nwords++], s);
    }
  fclose (stat_file);
  printf ("STAT Read %lu words from %s\n", stat_nwords, STAT_FILE);
}
#endif

int
main (int argc, char *argv[])
{
  while (argc >= 2 && argv[1][0] == '-')
    {
      if (argc == 2 && strcmp (argv[1], "-random") == 0) /* must be last */
        {
          seed = 1;
          argc --;
          argv ++;
        }
      else if (argc >= 3 && strcmp (argv[1], "-random") == 0)
        {
          seed = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 3 && strcmp (argv[1], "-nbest") == 0)
        {
          Nbest = atoi (argv[2]);
          argc -= 2;
          argv += 2;
        }
      else if (argc >= 2 && strcmp (argv[1], "-78joker") == 0)
        {
          type_partie = JOKER78;
          argc --;
          argv ++;
        }
      else if (argc >= 2 && strcmp (argv[1], "-joker") == 0)
        {
          type_partie = JOKER;
          argc --;
          argv ++;
        }
      else if (argc >= 2 && strcmp (argv[1], "-q") == 0)
        {
          verbose = -1;
          argc --;
          argv ++;
        }
      else if (argc >= 2 && strcmp (argv[1], "-interactive") == 0)
        {
          interactive = 1;
          argc --;
          argv ++;
        }
    }

#ifdef STAT_FILE
  read_stat_file ();
#endif

  read_dictionary ();
  // print_dictionary (dico->son, s, s);
  make_tableau ();
  joue ();
  clear_dictionary ();
}
