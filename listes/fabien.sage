# verification du fichier "tirages classiques"

load stat.sage
l6 = read_lettres(6)
l7 = read_lettres(7)
l8 = read_lettres(8)

# Example: check("lnrstae")
def check(s):
   print s + ":"
   global l7, l8
   assert len(s) == 7
   # sec
   for t in l7:
      if is_anagram(s,t):
         print "   ", t
	 sys.stdout.flush()
   # avec B, C, D, F, G, H, M, P, V
   for c in ['b','c','d','f','g','h','m','p','v']:
      for t in l8:
         if is_anagram(s+c,t):
	    print "   ", c, t
	    sys.stdout.flush()
   print
   sys.stdout.flush()

def checkall():
   for nv in [2..5]:
      nc = 7-nv
      for c in Subsets("lnrst",nc):
         p = ""
	 for x in c:
	    p += x
	 p = sort_word(p)
         for v in Subsets("aeiou",nv):
	    q = ""
	    for y in v:
	       q += y
	    q = sort_word(q)
	    check(p+q)

# put letter c in uppercase (assume appears only once)
def upper(mot,c):
   for i in range(len(mot)):
      if mot[i] == c:
         return mot[:i] + c.capitalize() + mot[i+1:]
   assert false

# Example: check("lnrsae")
def check6(s):
   print s + ":",
   global l6, l7
   assert len(s) == 6
   # sec
   n = 0
   for t in l6:
      if is_anagram(s,t):
         n += 1
         if n == 1:
            print t,
   if n > 1:
      print n,
   # avec B, C, D, F, G, H, M, P, V
   for c in ['b','c','d','f','g','h','m','p','v']:
      n = 0
      for t in l7:
         if is_anagram(s+c,t):
            n += 1
            if n == 1: # first
               mot_choisi = t
      if n > 0:
         mot_choisi = upper(mot_choisi,c)
         print mot_choisi,
         if n > 1:
            print n,
   print	    

def checkall6():
   for nv in [2..5]:
      nc = 6-nv
      for c in Subsets("lnrst",nc):
         p = ""
	 for x in c:
	    p += x
	 p = sort_word(p)
         for v in Subsets("aeiou",nv):
	    q = ""
	    for y in v:
	       q += y
	    q = sort_word(q)
	    check6(p+q)
   
