# return the list of n-letter words
def read_lettres(n):
   l = []
   fp = open("lettres_" + str(n),"r")
   while True:
      s = fp.readline()
      s = s.rstrip()
      if s == '':
         break
      l.append(s)
   fp.close()
   return l

def read_file(f):
   l = []
   fp = open(f,"r")
   while True:
      s = fp.readline()
      if s == '':
         break
      if s[0] == '#':
         continue
      s = s.rstrip()
      l.append(s)
   fp.close()
   return l

def write_file(l,f):
   fp = open(f,"w")
   for s in l:
      fp.write(s + '\n')
   fp.close()

def read_ods(n):
   return read_file("ods" + str(n))

# mots_invariables(4,'j')
def mots_invariables(n,c):
   l = read_lettres(n)
   ll = read_lettres(n+1)
   for s in l:
      if s.find(c) != -1:
         t = s + 's'
         if not t in ll:
            if not s[-1] in ['s','x','z']:
               print (s)

# return the words in l with "lettres cheres"
def lettres_cheres(l):
   L = []
   C = ['j','k','q','w','x','y','z']
   for s in l:
      if any(c in s for c in C):
         L.append(s)
   return L	 

# return the n-letter words with two "lettres cheres"
def lettres_cheres_2(n,only_ods8=false):
   # l = read_lettres(n)
   l = read_ods(8)
   l = [s for s in l if len(s)==n]
   if only_ods8:
      l7 = read_ods(7)
      l7 = [s for s in l7 if len(s)==n]
   c = ['j','k','q','w','x','y','z']
   n = 0
   for s in l:
      ok = false
      for i in range(len(c)):
         k = s.find(c[i])
         if k != -1:
            for j in range(len(c)):
               if s.find(c[j],k+1) != -1:
                  ok = true
                  break
         if ok:
            break
      if only_ods8:
         ok = ok and not s in l7
      if ok:
         print (s)
         n += 1
   print (n)

# 'abacule' -> 'aabcelu'
def sort_word(s):
   l = [s[i] for i in range(len(s))]
   l.sort()
   s = ''
   for x in l:
      s += x
   return s

def is_anagram(s,t):
   s = [s[i] for i in range(len(s))]
   t = [t[i] for i in range(len(t))]
   s.sort()
   t.sort()
   return s == t

def all_anagrams(s,l):
   u = sort_word(s)
   n = 0
   for t in l:
      if sort_word(t) == u:
         print (t)

# compute the probability of occurrence of s
def proba(s):
   l = [s[i] for i in range(len(s))]
   l.sort()
   i = 0
   p = 1
   T = dict([('a',9),('b',2),('c',2),('d',3),('e',15),('f',2),('g',2),('h',2),('i',8),('j',1),('k',1),('l',5),('m',3),('n',6),('o',6),('p',2),('q',1),('r',6),('s',6),('t',6),('u',6),('v',2),('w',1),('x',1),('y',1),('z',1)])
   # assert add(T[x] for x in T.keys()) == 100
   n = len(l)
   while i < n:
      j = i+1
      while j < n and l[i] == l[j]:
         j = j+1
      e = j-i
      p *= binomial(T[l[i]],e)
      i = j
   return p/binomial(102,n) # 1mot.net computes probabilities based on 102

# print the number of words with 1, 2, ..., anagrams
# l=read_lettres(7)
# do_stat(l)
def do_stat(l):
   l = [sort_word(s) for s in l]
   l.sort()
   T = dict()
   i = 0
   n = len(l)
   while i < n:
      j = i+1
      while j < n and l[i] == l[j]:
         j = j+1
      e = j-i # cardinality of l[i]
      if not e in T:
         T[e] = []
      T[e].append(l[i])
      i = j
   for e in T.keys():
      print (e,len(T[e]))
      pmax = 0
      pmin = infinity
      ptot = 0
      for s in T[e]:
         p = proba(s)
         ptot += p
         if p > pmax:
            pmax = p
            smax = s
         if p < pmin:
            pmin = p
            smin = s
      print ("pmax=", float(pmax), "s=", smax)
      print ("pmin=", float(pmin), "s=", smin)
      print ("ptot=", float(ptot))
   return T

def cmp(a,b):
   if a[1]<=b[1]:
      return int(-1)
   else:
      return int(1)

from functools import cmp_to_key

# extracts tirages with k anagrams
# print in outfile if given
# Example:
# l=read_ods(8)
# l=[s for s in l if len(s)==7]
# extract(l,2,outfile="/tmp/lettres_7-2")
def extract(l,k,outfile=None):
   l = [[w,sort_word(w)] for w in l]
   l.sort(key=cmp_to_key(cmp))
   L = []
   i = 0
   n = len(l)
   while i < n:
      j = i+1
      while j < n and l[i][1] == l[j][1]:
         j = j+1
      e = j-i # cardinality of l[i]
      if e==k:
         L.append([l[i][1],[l[t][0] for t in range(i,j)]])
      i = j
   if outfile==None:
      return L
   else:
      f = open(outfile,"w")
      for s in L:
         for t in s[1]:
            f.write(t + '\n')
      f.close()
      
# build a table of words with 2 "lettres cheres"
def cheres2(n,file):
   c = ['j','k','q','w','x','y','z']
   f = open (file, "w")
   f.write('\\documentclass{article}\\n')
   f.write("\\usepackage{fullpage}\\n")
   f.write("\\\\begin{document}\\n")
   f.write("\\\\begin{tabular}{|c|p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|p{2cm}|}\\hline\\n")
   f.write("&\\multicolumn{1}{c|}{j}&\\multicolumn{1}{c|}{k}&\\multicolumn{1}{c|}{q}&\\multicolumn{1}{c|}{w}&\\multicolumn{1}{c|}{x}&\\multicolumn{1}{c|}{y}&\\multicolumn{1}{c|}{z}\\\\\\\\\\hline\\n")
   l = read_lettres(n)
   T = dict()
   for a in c:
      for b in c:
         T[a,b] = []
   for w in l:
      for a in c:
         n = w.find(a)
         if n == -1:
           continue
         for b in c:
            m = w.find(b,n+1)
            if m != -1:
               T[a,b].append(w)
   for a in c:
      s = str(a)
      for b in c:
         if T[a,b]==[]:
            s = s + "&"
         else:
            s = s + "&" + str(T[a,b][0])
            for w in T[a,b][1:]:
               s = s + " " + str(w)
      s = s + "\\\\\\\\\\hline\\n"
      f.write(s)
   f.write("\\end{tabular}\\n")
   f.write("\\end{document}\\n")
   f.close()
   # print words with 3 lettres cheres
   for w in l:
      for a in c:
         if w.find(a) == -1:
            continue
         for b in c:
            if b==a or w.find(b) == -1:
               continue
            for d in c:
               if d != a and d != b and w.find(d) != -1:
                  print (a,b,d,w)
   print ()
   # print words which need a joker
   for a in c:
      for b in c:
         for w in T[a,b]:
            for d in Set({a,b}):
               n = w.find(d)
               assert n != -1
               m = w.find(d,n+1)
               if m != -1:
                  print (w)

def contains(s,w):
   ok = true
   for c in w:
      if s.find(c) == -1:
         return false
   return true

def complement(s,w):
   t = ''
   for c in s:
      if w.find(c) == -1:
         t += c
   return t

# split14('abehilmnopsuwz') page 28
# split14('abdehinoprsuwz') page 29
# split14('oabdefimprsuxy') page 36
# split14('acdefimnoqrstu') page 37
# split14('aefilmnoprstvy') page 42
# split14('zaeijklmnopstu') page 43
# split14('gadeimnorstuwx') page 52
# split14('aehlmnoprsuvxy') page 53
# split14('abcefinoprsuvx') page 60
# split14('vabcegiloprsuy') page 61
# split14('abehinoqstuvwz') page 70
# split14('abcehimoprsuwz') page 71
# split14('lacdeghijnorsy') page 80
# split14('abcegijknostux') page 81
def split14(s):
   l = read_lettres(7)
   for w in l:
      # the first letter should be in w
      if w.find(s[0]) == -1:
         continue
      if contains(s,w):
         t = complement(s,w)
         if len(t) == 7:
            for v in l:
               if is_anagram(v,t):
                  print (sort_word(w), sort_word(v))

def find_rectangle2(n):
   l = read_lettres(n)
   l2 = read_lettres(2)
   for u in l:
      for v in l:
         ok = true
         for i in range(n):
            x = u[i] + v[i]
            if not x in l2:
               ok = false
               break
         if ok:
            print (u, v)

def find_rectangle3(n):
   l = read_lettres(n)
   l3 = read_lettres(3)
   p2 = Set([x[:2] for x in l3]) # 2-prefixes
   count = 0
   for u in l:
      for v in l:
         ok = true
         for i in range(n):
            x = u[i] + v[i]
            if not x in p2:
               ok = false
               break
         if ok == false:
            continue
         for w in l:
            ok = true
            for i in range(n):
               x = u[i] + v[i] + w[i]
               if not x in l3:
                  ok = false
                  break
            if ok:
               count += 1
               print (count, u, v, w)

def find_rectangle4(n):
   l = read_lettres(n)
   l4 = read_lettres(4)
   p2 = Set([x[:2] for x in l4]) # 2-prefixes
   p3 = Set([x[:3] for x in l4]) # 3-prefixes
   for u in l:
      for v in l:
         ok = true
         for i in range(n):
            s = u[i] + v[i]
            if not s in p2:
               ok = false
               break
         if not ok:
            continue
         for w in l:
            ok = true
            for i in range(n):
               s = u[i] + v[i] + w[i]
               if not s in p3:
                  ok = false
                  break
            if not ok:
               continue
            for x in l:
               ok = true
               for i in range(n):
                  s = u[i] + v[i] + w[i] + x[i]
                  if not s in l4:
                     ok = false
                     break
               if ok:
                  print (u, v, w, x)

# find m words of n letters
# find_rectangle(5,7)
# 1 abaca babas aboli tonfa tunat unite seras
def find_rectangle(n,m):
   ln = read_lettres(n)
   lm = read_lettres(m)
   P = dict()
   for i in range(2,m+1):
      P[i] = Set([x[:i] for x in lm]) # i-prefixes
   l = [0 for i in range(m)] # current index
   k = 1 # invariant: the first k words are already chosen
   checked = true # the first k words are ok
   count = 0
   while true:
      if checked:
         if k == m:
            count += 1
            print (count,)
            for i in range(m):
               print (ln[l[i]],)
            print ()
         else: # k < m
            k += 1
            l[k-1] = 0
            checked = false
            continue
      else: # checked = false
         ok = true
         for i in range(n):
            s = ''
            for j in range(k):
               s += ln[l[j]][i]
            if not s in P[k]:
               ok = false
               break
         if ok:
            checked = true
            continue
      # increase counter
      while k >= 1 and l[k-1] + 1 == len(ln):
         k = k-1
      if k == 0:
         break
      l[k-1] += 1
      checked = bool(k == 1)
         
def six_plus_un():
   l = read_ods(8)
   l = [s for s in l if len(s)==7]
   f = open("/tmp/out","w")
   for s in l:
      ok = true
      l1 = 0
      l234 = 0
      for c in s:
         if c in ['j','k','q','w','x','y','z']:
            ok = false
         elif c in ['a','e','i','o','u','l','n','r','s','t']:
            l1 += 1
         elif c in ['b','c','d','f','g','h','m','p','v']:
            l234 += 1
         else:
            print (s)
            assert false
         if ok and l1==6 and l234==1:
            f.write(s + '\n')
   f.close()

def cat1(r):
   s = ''
   for c in r:
      s += c
   return s

def search_sept_plus_un_aux(s,rac=None,extra='bcdfghmpv'):
   if not (len(s)==7 or len(s)==8):
      return false
   l = [c for c in s if c in 'aeioulnrst']
   if len(Set(l))!=7:
      return false
   l2 = [c for c in s if not c in l]
   if len(l2)==0: # aeration
      if len(s)==8 or (rac!=None and Set(l)!=Set(rac)):
         return false
      return true
   if rac!=None and Set(l)!=Set(rac):
      return false
   if len(l2)!=1:
      print (s, l2)
   assert len(l2)==1, "len(l2)==1"
   c = l2[0]
   return c in extra

# cherche tirages de 7 lettres a un point (differentes)
# plus une autre lettre (2 points ou plus)
def search_sept_plus_un(rac=None,extra='bcdfghmpv',file=None):
   l = read_ods(8)
   if file!=None:
      file = open(file,"w")
   for r in Combinations('aeioulnrst',7):
      lr = [s for s in l if search_sept_plus_un_aux(s,rac=r,extra=extra)]
      s = cat1(r)
      for t in lr:
         s += ' ' + t
      if file==None:
         print (s)
      else:
         file.write(s+'\n')
         file.flush()
   if file!=None:
      file.close()

# split file f into f_x, f_y, ...
# where f_x contains all words with x
# and x has the smallest frequency in all words of f,
# then y has the smallest frequency in remaining words, ...
# split_freq("lettres_7-1")
# w 143
# k 512
# j 610
# q 651
# x 788
# y 903
# v 1351
# z 1526
# h 1472
# f 1461
# g 1596
# b 1374
# d 1163
# m 1008
# u 843
# p 611
# l 431
# c 276
# o 155
# r 78
# e 52
# s 22
# i 6
# t 2
# n 0
# a 0
def split_freq(f):
   l = read_file(f)
   s = 'abcdefghijklmnopqrstuvwxyz'
   s = Set([s[i] for i in range(26)])
   while len(s) > 0:
      m = infinity # min frequency
      for c in s:
         n = len([w for w in l if c in w])
         if n < m:
            m = n
            d = c
      print (d, m)
      g = open(f + '_' + d,"w")
      ld = [w for w in l if d in w]
      for w in ld:
         g.write(w + '\n')
      g.close()
      l = [w for w in l if not d in w]
      s = s - Set([d])

# renvoie k tirages steriles avec n lettres
def tirage_sterile(n,k):
   dico = read_ods(8)
   dico = Set([sort_word(s) for s in dico if len(s)==n])
   l = []
   T = dict([('a',9),('b',2),('c',2),('d',3),('e',15),('f',2),('g',2),('h',2),('i',8),('j',1),('k',1),('l',5),('m',3),('n',6),('o',6),('p',2),('q',1),('r',6),('s',6),('t',6),('u',6),('v',2),('w',1),('x',1),('y',1),('z',1)])
   S = []
   for c in T:
      for i in range(T[c]):
         S.append(c)
   while len(l)<k:
      s = ''
      for i in range(n):
         s += S[ZZ.random_element(len(S))]
         s = sort_word(s)
      if not s in dico:
         l.append(s)
   return l

# renvoie tous les tirages steriles de n lettres avec k points
# toutes lettres differentes
def tirage_sterile2(n,k,out=None,min_vowels=2,min_consonants=2,repetitions=1):
   if out!=None:
      out = open(out,"w")
   dico = read_ods(8)
   dico = Set([sort_word(s) for s in dico if len(s)==n])
   T = dict([('a',1),('b',3),('c',3),('d',2),('e',1),('f',4),('g',2),('h',4),('i',1),('j',8),('k',10),('l',1),('m',2),('n',1),('o',1),('p',3),('q',8),('r',1),('s',1),('t',1),('u',1),('v',4),('w',10),('x',10),('y',10),('z',10)])
   # select possible letters
   S = dict()
   for c in T:
      if T[c]+n-1 <= k:
         S[c] = T[c]
   A = [c for c in S]
   L = []
   vowels = 'aeiou'
   A = A*repetitions
   for x in Combinations(A,n):
      # check numbers of points
      t = add(S[c] for c in x)
      if t!=k:
         continue
      # check minimum number of vowels
      nv = add([1 for c in x if c in vowels])
      if nv<min_vowels:
         continue
      # check minimum number of consonants
      nc = n-nv
      if nc<min_consonants:
         continue
      s = ''
      for c in x:
         s += c
      s = sort_word(s)
      if not s in dico:
         L.append(s)
   if out==None:
      return L
   else:
      for s in L:
         out.write(s+'\n')
      out.close()

