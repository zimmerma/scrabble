def read_file(f):
   l = []
   fp = open(f,"r")
   while True:
      s = fp.readline()
      if s == '':
         break
      if s[0] == '#':
         continue
      s = s.rstrip()
      l.append(s)
   fp.close()
   return l

l = read_file("lettres_3")

# return all words in l that differ by s only by letter i
def cousins(s,l,i):
   L = []
   u = [j for j in range(len(s)) if j!=i]
   for t in l:
      ok = true
      for j in u:
         if s[j]!=t[j]:
            ok = false
      if ok:
         L.append(t)
   return L

# return all words in l that differ by s only by letter i
# assuming s[i] is a vowel and other words have a vowel
def cousins_vowels(s,l,i):
   assert s[i] in 'aeiou', "s[i] in 'aeiou'"
   L = []
   u = [j for j in range(len(s)) if j!=i]
   for t in l:
      ok = t[i] in 'aeiou'
      for j in u:
         if s[j]!=t[j]:
            ok = false
      if ok:
         L.append(t)
   return L

# return the word having the largest number of cousins which differ by
# a vowel
def largest_cousins_vowel(L):
   lmax = []
   for s in L:
      for i in range(3):
         if s[i] in 'aeiou':
            l = cousins_vowels(s,L,i)
            if len(l)>len(lmax):
               lmax = l
               smax = s
   return smax, lmax

% ('are', ['are', 'ere', 'ire', 'ore', 'ure'])

